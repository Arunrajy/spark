import { Directive, ElementRef,HostListener } from '@angular/core';

@Directive({
  selector: '[appClickStop]'
})
export class ClickstopDirective {
  @HostListener("document:click", ["$event"])
  clickout(event) {
    if(this.eRef.nativeElement.contains(event.target)) {
      if(event.target != document.getElementById('cart-dropdown-menu-id')) {
        document.getElementById('cart-dropdown-menu-id').classList.add("opened");          
        document.getElementById('cart-dropdown-click-id').classList.add("eventdisabled");          
    } 
    } else {
      if(event.target != document.getElementById('cart-dropdown-menu-id')) {
        document.getElementById('cart-dropdown-menu-id').classList.remove("opened");
        document.getElementById('cart-dropdown-click-id').classList.remove("eventdisabled");          
    } 
    }
  }
  constructor(private eRef: ElementRef) {

  }


}
