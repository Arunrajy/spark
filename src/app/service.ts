import { Injectable, Inject, PLATFORM_ID  } from "@angular/core";
import { HttpClient , HttpHeaders,HttpErrorResponse} from '@angular/common/http';
import { BehaviorSubject,Subject,Observable,throwError  } from 'rxjs';
import { catchError,map } from 'rxjs/operators'
import { Router, NavigationEnd } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';
import { environment } from './../environments/environment';

@Injectable()
export class Service {
  configUrl = environment.configUrl;
  configSubUrl = environment.configSubUrl;
  baseUrl =  environment.baseUrl;
  placeholderImage = environment.placeHolderImage;
  private messageSource = new BehaviorSubject('');
  headers: any;

  currentMessage = this.messageSource.asObservable();
  
  private _loading: boolean = false;
  loadingStatus = new Subject();
  constructor(
    private http: HttpClient,@Inject(PLATFORM_ID) private platformId: Object,
    private router: Router

  ) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
  }

  get loading():boolean {
    return this._loading;
  }

  set loading(value) {
    this._loading = value;
    this.loadingStatus.next(value);
  }

  startLoading() {
    this.loading = true;
  }

  stopLoading() {
    this.loading = false;
  }

  getConfig(particle_url): Observable<any> {
    return this.http.get(this.configUrl+particle_url,{headers: this.headers}).pipe(
      map((res) => {
        if (!res) {
          throw new Error('Value expected!');
        }
        return res;
      }),
      catchError(this.handleError)
    );
  }

  postConfig(particle_url: any, obj: any): Observable<any> {
    return this.http.post(this.configUrl + particle_url,obj,{headers: this.headers}).pipe(
      map(res => {
        if (!res) {
          throw new Error('Value expected!');
        }
        return res;
      }),
      catchError(this.handleError)
    );
  }
  getBase(particle_url): Observable<any> {
    return this.http.get(this.baseUrl+particle_url,{headers: this.headers}).pipe(
      map((res) => {
        if (!res) {
          throw new Error('Value expected!');
        }
        return res;
      }),
      catchError(this.handleError)
    );
  }

  postBase(particle_url: any, obj: any): Observable<any> {
    return this.http.post(this.baseUrl + particle_url,obj,{headers: this.headers}).pipe(
      map(res => {
        if (!res) {
          throw new Error('Value expected!');
        }
        return res;
      }),
      catchError(this.handleError)
    );
  }

  handleError(error: HttpErrorResponse){
    return throwError(error);
    }

  changeMessage(message: string) {
    this.messageSource.next(message)
  }
  
  setScrollTop() {
    if (isPlatformBrowser(this.platformId)) {
      this.router.events.subscribe((event: NavigationEnd) => {
        window.scroll(0, 0);
      });
    }
  }

  // update and replace url
  updateQueryParams(value:any, activeUrl:any, location:any){
    const urlTree = this.router.createUrlTree([], {
      relativeTo: activeUrl,
      queryParams: {
        search: value
      },
      queryParamsHandling: 'merge',
  });
  location.go(urlTree.toString().replace(/\?/g, "/#"));
  }

  //convert object to query string for get action
  convertObjecttoQueryString(data:any){
    let querystring = Object.keys(data).map(function(key) { 
        return key + '=' + data[key]; 
      }).join('&'); 
      return querystring;
    }

  // convert Date format
  GetFormattedDate(date:string) {
    let getDate = new Date(date);
    let monthList = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    let month = monthList[getDate.getMonth()];
    let day = getDate.getDate();
    let year = getDate.getFullYear();
    return  `${day}th ${month}, ${year}`;
  }

    //truncate long string
    truncate(input:string, wordLimit:number) {
    if (input.length > wordLimit)
        return input.substring(0,wordLimit) + '...';
    else
        return input;
  };

  //convert string to slugify
  slugify(str: string): string {
    return this.isString(str)
      ? str.toLowerCase().trim()
        .replace(/[^\w\-]+/g, ' ')
        .replace(/\s+/g, '-')
      : str;
  }
  isString(value: any) {
    return typeof value === 'string';
  }

}
