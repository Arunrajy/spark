import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { TopdomainComponent } from './components/topdomain/topdomain.component';
import { SearchComponent } from './components/search/search.component';
import { DomainsuggestionComponent } from './components/domainsuggestion/domainsuggestion.component';
import { DomainauctionComponent } from './components/domainauction/domainauction.component';
import { ExpireddomainComponent } from './components/expireddomain/expireddomain.component';
import { DomainNamesComponent} from './components/domain-names/domain-names.component';
import { BrandingComponent} from './components/branding/branding.component';
import { SinglePostComponent } from './components/single-post/single-post.component';
import { FaqComponent} from './components/faq/faq.component';
import { PrivacyComponent} from './components/privacy/privacy.component';
import { TermsofUseComponent} from './components/terms-of-use/terms-of-use.component';
import { NotFoundComponent} from './components/404/not-found.component';
const routes: Routes = [
{ 
  path: '', redirectTo: '/home', pathMatch: 'full'
},
{ path: 'home', component: HomeComponent,
},
{
  path: 'home/:name/:id',component: SinglePostComponent,
},
{
  path: 'search',component: SearchComponent,
},
{
  path: 'topdomain',component: TopdomainComponent,
},
{
  path: 'domainsuggestion',component: DomainsuggestionComponent,
},
{
  path: 'domainauction',component: DomainauctionComponent,
},
{
  path: 'expireddomain',component: ExpireddomainComponent,
},
{
  path: 'domain-marketing',component: DomainNamesComponent,
},
{
  path: 'domain-marketing/:name/:id',component: SinglePostComponent,
},
{
  path: 'branding',component: BrandingComponent,
},
{
  path: 'branding/:name/:id',component: SinglePostComponent,
},
{
  path: 'faq',component: FaqComponent,
},
{
  path: 'privacy',component: PrivacyComponent,
},
{
  path: 'terms-of-use',component: TermsofUseComponent,
},
{
  path: '404',component: NotFoundComponent,
},

   // otherwise redirect to home
   { path: '**', redirectTo: '404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
