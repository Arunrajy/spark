import { BrowserModule } from '@angular/platform-browser';
import { NgModule,enableProdMode } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { Service } from './service';
import { ClickstopDirective } from './clickstop.directive';
import { TopdomainComponent } from './components/topdomain/topdomain.component';
import { SearchComponent } from './components/search/search.component';
import { DomainsuggestionComponent } from './components/domainsuggestion/domainsuggestion.component';
import { DomainauctionComponent } from './components/domainauction/domainauction.component';
import { ExpireddomainComponent } from './components/expireddomain/expireddomain.component';
import { DomainNamesComponent} from './components/domain-names/domain-names.component';
import { BrandingComponent} from './components/branding/branding.component';
import { SinglePostComponent } from './components/single-post/single-post.component';
import { FaqComponent} from './components/faq/faq.component';
import { PrivacyComponent} from './components/privacy/privacy.component';
import { TermsofUseComponent} from './components/terms-of-use/terms-of-use.component';
import { NotFoundComponent} from './components/404/not-found.component';
import { SharedModule} from './components/shared-components/shared.module';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr-FR');

enableProdMode();
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ClickstopDirective,
    TopdomainComponent,
    SearchComponent,
    DomainsuggestionComponent,
    DomainauctionComponent,
    ExpireddomainComponent,
    DomainNamesComponent,
    BrandingComponent,
    SinglePostComponent,
    FaqComponent,
    PrivacyComponent,
    TermsofUseComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [Service],
  bootstrap: [AppComponent]
})
export class AppModule { }
