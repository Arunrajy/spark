import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomainsuggestionComponent } from './domainsuggestion.component';

describe('DomainsuggestionComponent', () => {
  let component: DomainsuggestionComponent;
  let fixture: ComponentFixture<DomainsuggestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomainsuggestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomainsuggestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
