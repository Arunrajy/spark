import { Component, OnInit} from '@angular/core';
import { Service } from '../../service';
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
@Component({
  selector: 'app-branding',
  templateUrl: './branding.component.html',
  styleUrls: ['./branding.component.css']
})

export class BrandingComponent implements OnInit {

  domainName:string = 'speedydomainsearch';
  brandName:string = 'branding';
  apiSubUrl:string = this.service.configSubUrl + 'latest-posts/?';
  imagePlaceholderUrl:string = this.service.placeholderImage + '?text=branding';
  brandings:any=[];
  showLoading:any;
  loadButtonHide:boolean;
  showList:number = 3;
  loadingSubscription: Subscription;
  errorResponse:string;
  constructor(private service:Service,private router: Router) {
   }

  ngAfterViewInit() {
  }

  ngOnInit() {
    this.initializePreloading();
    this.getList();
  }

  //initialize prelaoding
  initializePreloading(){
    this.showLoading = true;
    this.loadingSubscription = this.service.loadingStatus.subscribe((value) => {
      this.showLoading = value;
    });
  }

  // get list
  getList(){
    let requestData = {
      domain:this.domainName,
      category:this.brandName
    };

    let queryString = this.service.convertObjecttoQueryString(requestData);
    this.service.getBase(this.apiSubUrl + queryString)
    .subscribe((responseData: any) => {
      if(responseData != null || responseData != undefined){
        this.service.stopLoading();
        let brandingData=[];
        responseData.forEach((element: { postdate: any; postimage:any, title: any; content: any; id:any}) => {
          let imageUrl:any;
          if(element.postimage == false){
            imageUrl =  this.imagePlaceholderUrl
           }
           else{
             imageUrl = element.postimage;
           }
          let dataConstruct:Config = {
            date: this.service.GetFormattedDate(element.postdate),
            img: imageUrl,
            title: element.title,
            description: this.service.truncate(element.content,100),
            id:element.id
          }
          brandingData.push(dataConstruct);
        });
        this.brandings = brandingData;
        this.checkLoadMore();
      }else{
        this.service.startLoading();
      }
    },(errorResponse)=>{
      if(errorResponse.status == 0){
        this.errorResponse = errorResponse.message;
        this.service.stopLoading();
      }else if(errorResponse.error.data.status == 404){
        this.errorResponse = errorResponse.error.message;
        this.service.stopLoading();
      }
    });
  }

   // loadmore functionality
  loadMore(){
    this.showList += 3;
    this.checkLoadMore();
  }

  // checkLoadMore to show/hide
  checkLoadMore(){
    if(this.brandings.length <= this.showList){
      this.loadButtonHide=true;
    }else{
      this.loadButtonHide=false;
    }
  }

  openSubPage(name:string,id:number){
    this.router.navigate(['branding',this.service.slugify(name),id])
  }

}

export interface Config {
  date: any;
  img: any;
  title:any;
  description:any
  id:any;
}