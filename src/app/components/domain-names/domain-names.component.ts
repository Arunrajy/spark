import { Component, OnInit} from '@angular/core';
import { Service } from '../../service';
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
@Component({
  selector: 'app-domain-names',
  templateUrl: './domain-names.component.html',
  styleUrls: ['./domain-names.component.css']
})

export class DomainNamesComponent implements OnInit {
  domainName:string = 'speedydomainsearch';
  brandName:string = 'domain-marketing';
  apiSubUrl:string = this.service.configSubUrl + 'latest-posts/?';
  imagePlaceholderUrl:string = this.service.placeholderImage + '?text=domain-marketing';
  domainMarketings:any=[];
  showLoading:any;
  loadButtonHide:boolean;
  showList:number = 3;
  loadingSubscription: Subscription;
  imageUrl:any;
  errorResponse:string;
  constructor(private service:Service,private router: Router) {
   }

   ngAfterViewInit() {
  }

  ngOnInit() {
    this.initializePreloading();
    this.getList();
  }

  //initialize prelaoding
  initializePreloading(){
    this.showLoading = true;
    this.loadingSubscription = this.service.loadingStatus.subscribe((value) => {
      this.showLoading = value;
    });
  }
  
  // get list
  getList(){
    let requestData = {
      domain:this.domainName,
      category:this.brandName
    };

    let queryString = this.service.convertObjecttoQueryString(requestData);
    this.service.getBase(this.apiSubUrl + queryString)
    .subscribe((responseData: any) => {
      if(responseData != null || responseData != undefined){
        this.service.stopLoading();
        let brandingData=[];
        responseData.forEach((element: { postdate: any; postimage:any, title: any; content: any; id:any}) => {
          if(element.postimage == false){
           this.imageUrl =  this.imagePlaceholderUrl
          }
          else{
            this.imageUrl = element.postimage;
          }
          
          let dataConstruct:Config = {
            date: this.service.GetFormattedDate(element.postdate),
            img: this.imageUrl,
            title: element.title,
            description: this.service.truncate(element.content,100),
            id:element.id
          }
          brandingData.push(dataConstruct);
        });
        this.domainMarketings = brandingData;
        this.checkLoadMore();
      }else{
        this.service.startLoading();
      }
    },(errorResponse)=>{
      if(errorResponse.status == 0){
        this.errorResponse = errorResponse.message;
        this.service.stopLoading();
      }else if(errorResponse.error.data.status == 404){
        this.errorResponse = errorResponse.error.message;
        this.service.stopLoading();
      }
    });
  }

  // loadmore functionality
  loadMore(){
    this.showList += 3;
  }

  // checkLoadMore to show/hide
  checkLoadMore(){
    if(this.domainMarketings.length <= this.showList){
      this.loadButtonHide=true;
    }else{
      this.loadButtonHide=false;
    }
  }

  openSubPage(name:string,id:number){
    this.router.navigate(['domain-marketing',this.service.slugify(name),id])
  }

}
export interface Config {
  date: any;
  img: any;
  title:any;
  description:any
  id:any;
}