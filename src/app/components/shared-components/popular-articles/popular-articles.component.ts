import { Component, OnInit} from '@angular/core';
import { Service } from '../../../service';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';
import { Subscription } from "rxjs";
@Component({
  selector: 'app-popular-articles',
  templateUrl: './popular-articles.component.html',
  styleUrls: ['./popular-articles.component.css']
})

export class popularArticlesComponent implements OnInit {

  domainName:string = 'speedydomainsearch';
  apiPopularPostUrl:string = this.service.configSubUrl + 'popular-posts/?';
  imagePlaceholderUrl:string = this.service.placeholderImage + '?text=branding';
  popularArticles:any=[];
  showLoading:any;
  loadingSubscription: Subscription;
  errorResponse:string;
  constructor(private service:Service,private router: Router,private activeroute: ActivatedRoute) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    }
   }

  ngAfterViewInit() {
  }

  ngOnInit() {
    this.initializePreloading();
    this.getPopularList();
  }

  //initialize prelaoding
  initializePreloading(){
    this.showLoading = true;
    this.loadingSubscription = this.service.loadingStatus.subscribe((value) => {
      this.showLoading = value;
    });
  }

  getPopularList(){
    let requestData = {
      domain:this.domainName
    };
    let queryString = this.service.convertObjecttoQueryString(requestData);
    this.service.getBase(this.apiPopularPostUrl + queryString)
    .subscribe((responseData: any) => {
      if(responseData != null || responseData != undefined){
        let popularArticle = [];
        responseData.forEach((element: { postdate: any; postimage:any, title: any; content: any; id:any}) => {
          let imageUrl:any;
          if(element.postimage == false){
            imageUrl =  this.imagePlaceholderUrl
           }
           else{
             imageUrl = element.postimage;
           }
          let dataConstruct:Config = {
            date: this.service.GetFormattedDate(element.postdate),
            img: imageUrl,
            title: element.title,
            description: this.service.truncate(element.content,100),
            id:element.id
          }
          popularArticle.push(dataConstruct);
        });
        this.popularArticles = popularArticle;
      }else{

      }
    },(errorResponse)=>{
      if(errorResponse.status == 0){
        this.errorResponse = errorResponse.message;
      }else if(errorResponse.error.data.status == 404){
        this.errorResponse = errorResponse.error.message;
      }
    })
  }
  openSubPage(name:string,id:number){
    let href =this.activeroute.snapshot.url[0].path;
    this.router.navigate([href,this.service.slugify(name),id])
  }
}

export interface Config {
  date: any;
  img: any;
  title:any;
  description:any
  id:any;
}