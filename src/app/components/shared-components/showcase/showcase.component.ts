import { Component, OnInit,Input} from '@angular/core';
import { Service } from '../../../service';
import { Subscription } from "rxjs";
@Component({
  selector: 'app-showcase',
  templateUrl: './showcase.component.html',
  styleUrls: ['./showcase.component.css']
})

export class showcaseComponent implements OnInit {

  domainName:string = 'speedydomainsearch';
  @Input() spaceName:string;
  apiShowcaseUrl:string = this.service.configSubUrl + 'showcase/?';
  @Input() count:number;
  imagePlaceholderUrl:string = this.service.placeholderImage +'?text=banner';
  showCase:any=[];
  showLoading:any;
  loadingSubscription: Subscription;
  errorResponse:string;
  @Input() minValue:any;
  @Input() maxValue:any;
  constructor(private service:Service) {
  }

  ngAfterViewInit() {
  }

  ngOnInit() {
  this.getPopularList();
  }

  getPopularList(){
    let requestData = {
      domain:this.domainName,
      spacename:this.spaceName,
      count:this.count
    };
    let queryString = this.service.convertObjecttoQueryString(requestData);
    this.service.getBase(this.apiShowcaseUrl + queryString)
    .subscribe((responseData: any) => {
      if(responseData != null || responseData != undefined){
        let showCase = [];
        responseData.forEach((element: { postdate: any; postimage:any, title: any; content: any; link:any, id:any}) => {
          let imageUrl:any;
          if(element.postimage == false){
            imageUrl =  this.imagePlaceholderUrl
           }
           else{
             imageUrl = element.postimage;
           }
          let dataConstruct:Config = {
            date: this.service.GetFormattedDate(element.postdate),
            img: imageUrl,
            title: element.title,
            description: this.service.truncate(element.content,100),
            link:element.link,
            id:element.id
          }
          showCase.push(dataConstruct);
        });
        this.showCase = showCase;
      }
    },(errorResponse)=>{
      if(errorResponse.status == 0){
        this.errorResponse = errorResponse.message;
      }else if(errorResponse.error.data.status == 404){
        this.errorResponse = errorResponse.error.message;
      }
    })
  }

}

export interface Config {
  date: any;
  img: any;
  title:any;
  description:any;
  link:any;
  id:any;
}