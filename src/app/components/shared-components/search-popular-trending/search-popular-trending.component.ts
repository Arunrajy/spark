import { Component, OnInit,Input} from '@angular/core';
import { Service } from '../../../service';
import { Subscription } from "rxjs";
@Component({
  selector: 'app-search-popular-trending',
  templateUrl: './search-popular-trending.component.html',
  styleUrls: ['./search-popular-trending.component.css']
})

export class SearchPopularTrendingComponent implements OnInit {

  searchPopularOrTrendings:any;
  domainItems:any = [];
  @Input() SearchType:string;
  @Input() SearchLimit:number;
  searchapi:string = 'search-ds';
  domainName:string = 'abc.com';
  pageTitle:any;
  showLoading:any;
  loadingSubscription: Subscription;
  errorResponse:string;
  showList:number;
  constructor(private service:Service) {
  }

  ngAfterViewInit() {
  }

  ngOnInit() {
    this.initializePreloading();
    this.getPopularTrending();
  }
  
  //initialize prelaoding
  initializePreloading(){
    this.showLoading = true;
    this.loadingSubscription = this.service.loadingStatus.subscribe((value) => {
      this.showLoading = value;
    });
  }

  getPopularTrending(){
    let requestData = {
      passedDomain:this.domainName
    };

    this.service.postConfig(this.searchapi,requestData)
    .subscribe((responseData: Config) => {
      if(responseData != null || responseData != undefined){
        this.pageTitle = this.SearchType;
        let searchPopularOrTrending = [];
        JSON.parse(responseData[this.SearchType]).forEach((element: { period: any; trend_percent:any, keyword: any; total_count: any;}) => {
          let date:any = '';
          let trend:any = '';
          let total:any = '';
          if(element.period && element.trend_percent){
            date = element.period;
            trend = element.trend_percent;
          }else if(element.total_count){
            total = element.total_count;
          }
          let dataConstruct = {
            period: date,
            keyword: element.keyword,
            trend_percent: trend,
            total_count:total
          }
          searchPopularOrTrending.push(dataConstruct);
        });
        this.searchPopularOrTrendings = searchPopularOrTrending;
        this.showList = this.SearchLimit;
      }else{
      }
    },(errorResponse)=>{
      if(errorResponse.status == 0){
        this.errorResponse = errorResponse.message;
      }else if(errorResponse.error.data.status == 404){
        this.errorResponse = errorResponse.error.message;
      }
    })
  }
}

export interface Config {
  popular: any;
  trending: any;
}
