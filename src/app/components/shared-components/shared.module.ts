import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { popularArticlesComponent} from './popular-articles/popular-articles.component';
import { showcaseComponent} from './showcase/showcase.component';
import { SearchPopularTrendingComponent } from './search-popular-trending/search-popular-trending.component';


@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        popularArticlesComponent,
        showcaseComponent,
        SearchPopularTrendingComponent
    ],
    exports: [
        popularArticlesComponent,
        showcaseComponent,
        SearchPopularTrendingComponent
    ]
})
export class SharedModule {

}
