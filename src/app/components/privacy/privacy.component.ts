import { Component, OnInit,ViewEncapsulation} from '@angular/core';
import { Service } from '../../service';
import { Subscription } from "rxjs";
@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./privacy.component.css']
})

export class PrivacyComponent implements OnInit {
  domainName:string = 'speedydomainsearch';
  brandName:string = 'general';
  article:string = 'privacy';
  apiSubUrl:string = this.service.configSubUrl + 'latest-posts/?';
  privacy:any=[];
  showLoading:any;
  loadingSubscription: Subscription;
  errorResponse:string;
  constructor(private service:Service) {
   }

  ngAfterViewInit() {
  }

  ngOnInit() {
    this.initializePreloading();
    this.getList();
  }

  //initialize prelaoding
  initializePreloading(){
    this.showLoading = true;
    this.loadingSubscription = this.service.loadingStatus.subscribe((value) => {
      this.showLoading = value;
    });
  }

  // get list
  getList(){
    let requestData = {
      domain:this.domainName,
      category:this.brandName,
      article: this.article
    };

    let queryString = this.service.convertObjecttoQueryString(requestData);
    this.service.getBase(this.apiSubUrl + queryString)
    .subscribe((responseData: any) => {
      if(responseData != null || responseData != undefined){
        this.service.stopLoading();
        let brandingData=[];
        responseData.forEach((element: { post_date: any; title: any; content: any; id:any}) => {
          let dataConstruct:Config = {
            date: this.service.GetFormattedDate(element.post_date),
            title: element.title,
            description: element.content,
            id:element.id
          }
          brandingData.push(dataConstruct);
        });
        this.privacy = brandingData;
      }else{
        this.service.startLoading();
      }
    },(errorResponse)=>{
      if(errorResponse.status == 0){
        this.errorResponse = errorResponse.message;
        this.service.stopLoading();
      }else if(errorResponse.error.data.status == 404){
        this.errorResponse = errorResponse.error.message;
        this.service.stopLoading();
      }
    });
  }

}

export interface Config {
  date: any;
  title:any;
  description:any
  id:any;
}