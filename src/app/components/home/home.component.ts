import { Component, OnInit,ViewChildren} from '@angular/core';
import { Location,LocationStrategy,HashLocationStrategy} from '@angular/common';
import { Service } from '../../service';
import { Router } from "@angular/router";
import { Subscription } from "rxjs";

declare var require: any;
const data: any = require('./home.json');

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [
    Location,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
  ]
})

export class HomeComponent implements OnInit {
  domainobjects:any;
  domainItems:any = [];
  searchValue:any="";
  searchDomainUrl:string;
  message:string;
  searchapi:string = 'search-test';
  domainadded:any;
  domainnotadded:any;
  routerurl:any ;
  @ViewChildren('input') vc;
  domainName:string = 'speedydomainsearch';
  apiSubUrl:string =  this.service.configSubUrl + 'highlight-posts/?';
  imagePlaceholderUrl:string = this.service.placeholderImage + '?text=highlights';
  highlights:any=data;
  showLoading:any;
  loadingSubscription: Subscription;
  errorResponse:string;

  constructor(private service:Service,private router: Router, private location: Location) {
  }

  ngAfterViewInit() {
    this.vc.first.nativeElement.focus();
  }

  ngOnInit() {
    this.initializePreloading();
    this.getList();
    this.service.currentMessage.subscribe(message => this.message = message)
  }

  //initialize prelaoding
  initializePreloading(){
    this.showLoading = true;
    this.loadingSubscription = this.service.loadingStatus.subscribe((value) => {
      this.showLoading = value;
    });
  }

  onKeyDown(event: any){
    if (event.which  === 32) { 
      return false;
    }
  }

  onKey(event: any, switchname:any) {
    if(!event.target.value){
      console.log("empty");
    }else{
      this.searchDomainUrl=event.target.value;
      if(switchname == 'top-domain'){
        this.routerurl = '/topdomain';
      }else if(switchname == 'domain-suggestion'){
        this.routerurl = '/domainsuggestion';
      }else if(switchname == 'domain-auction'){
        this.routerurl = '/domainauction';
      }else if(switchname == 'expired-domain'){
        this.routerurl = '/expireddomain';
      }else{
        this.routerurl = '/search';
      }
      this.router.navigate([this.routerurl],{ queryParams : {search:this.searchDomainUrl}});
    }
}

onSubmit(value,switchname){
  if(value.length != 0){
  let data = {
    passedDomain:value
  };
  this.service.postConfig(this.searchapi,data)
  .subscribe((data: Config) => {
      this.domainobjects = {
        domainAvailability: data['availability'],
        topdomain: JSON.parse(data['cctld']),
        suggestions: JSON.parse(data['spin']),
        auction:  JSON.parse(data['extension']),
    }
  });
}else{
  console.log("value is empty");
}
  return false;
}

  // get list
  getList(){
    let requestData = {
      domain:this.domainName,
    };

    let queryString = this.service.convertObjecttoQueryString(requestData);
    this.service.getBase(this.apiSubUrl + queryString)
    .subscribe((responseData: any) => {
      if(responseData != null || responseData != undefined){
        this.service.stopLoading();
        let highlightsData=[];
        responseData.forEach((element: { postimage:any, category:any, title: any; content: any; id:any}, index) => {
          let imageUrl:any;
          if(element.postimage == false){
            imageUrl =  this.imagePlaceholderUrl
           }
           else{
             imageUrl = element.postimage;
           }
           let trucatedData:any;
           if(index == 0){
            trucatedData =   this.service.truncate(element.content,300)
           }else{
            trucatedData =   this.service.truncate(element.content,160)

           }
          let dataConstruct:Config2 = {
            img: imageUrl,
            title: element.title,
            category: element.category,
            description: trucatedData,
            id:element.id
          }
          highlightsData.push(dataConstruct);
        });
        this.highlights = highlightsData;
      }else{
        this.service.startLoading();
      }
    },(errorResponse)=>{
      if(errorResponse.status == 0){
        this.errorResponse = errorResponse.message;
        this.service.stopLoading();
      }else if(errorResponse.error.data.status == 404){
        this.errorResponse = errorResponse.error.message;
        this.service.stopLoading();
      }
    });
  }

  openSubPage(name:string,id:number){
    this.router.navigate(['home',this.service.slugify(name),id])
  }

}

export interface Config {
  availability: any;
  cctld: any;
  spin: any;
  extension: any;
}

export interface Config2 {
  img: any;
  title:any;
  category:any;
  description:any
  id:any;
}

