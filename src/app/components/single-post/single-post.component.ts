import { Component, OnInit} from '@angular/core';
import { Service } from '../../service';
import { Title } from '@angular/platform-browser';
import { Router,NavigationEnd  } from "@angular/router";
import { ActivatedRoute } from '@angular/router';
import { Subscription } from "rxjs";
@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.css']
})

export class SinglePostComponent implements OnInit {
  domainName:string = 'speedydomainsearch';
  apiSubUrl:string = this.service.configSubUrl + 'single-post/?';
  imagePlaceholderUrl:string = this.service.placeholderImage +'?text=Single Post';
  singlePost:any=[];
  showLoading:any;
  loadButtonHide:boolean;
  showList:number = 3;
  loadingSubscription: Subscription;
  imageUrl:any;
  errorResponse:string;
  singlePostID:number;
  pageTitle:string;
  constructor(private service:Service,private router: Router,private activeroute: ActivatedRoute,private titleService: Title ) {
    this.activeroute.params.subscribe((res) =>{
      this.singlePostID = res.id;
    });
    if(this.activeroute.snapshot.url[0].path == 'domain-marketing'){
      this.pageTitle = "Domain Marketing";
    }else if(this.activeroute.snapshot.url[0].path == 'branding'){
      this.pageTitle = "Branding"; 
    }
   }

   ngAfterViewInit() {
  }

  ngOnInit() {
    this.initializePreloading();
    this.getList();
  }

  //initialize prelaoding
  initializePreloading(){
    this.showLoading = true;
    this.loadingSubscription = this.service.loadingStatus.subscribe((value) => {
      this.showLoading = value;
    });
  }

  // get list
  getList(){
    let requestData = {
      domain:this.domainName,
      article_id:this.singlePostID
    };

    let queryString = this.service.convertObjecttoQueryString(requestData);
    this.service.getBase(this.apiSubUrl + queryString)
    .subscribe((responseData: any) => {
      if(responseData != null || responseData != undefined){
        this.service.stopLoading();
        let singleData=[];
        responseData.forEach((element: { postdate: any; postimage:any, title: any; content: any; id:any}) => {
          if(element.postimage == false){
           this.imageUrl =  this.imagePlaceholderUrl
          }
          else{
            this.imageUrl = element.postimage;
          }
          let dataConstruct:Config = {
            date: this.service.GetFormattedDate(element.postdate),
            img: this.imageUrl,
            title: element.title,
            description: element.content,
            id:element.id
          }
          singleData.push(dataConstruct);
        });
        this.singlePost = singleData;
        // check valid url name
        this.activeroute.params.subscribe((res) =>{
          if(this.checkName(res.name) == false){
            this.router.navigate(['404']);
          }
        })
      }else{
        this.service.startLoading();
      }
    },(errorResponse)=>{
      if(errorResponse.status == 0){
        this.errorResponse = errorResponse.message;
        this.service.stopLoading();
      }else if(errorResponse.error.data.status == 404){
        this.errorResponse = errorResponse.error.message;
        this.service.stopLoading();
      }
    });
  }

  checkName(getName:string){
    if(getName === this.service.slugify(this.singlePost[0].title)){
      return true;
    }else{
      return false;
    }
  }
    
}
export interface Config {
  date: any;
  img: any;
  title:any;
  description:any
  id:any;
}