import { Component, OnInit} from '@angular/core';
import { Service } from '../../service';
import { Subscription } from "rxjs";
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})

export class FaqComponent implements OnInit {


  domainName:string = 'speedydomainsearch';
  brandName:string = 'faq';
  apiSubUrl:string = this.service.configSubUrl + 'latest-posts/?';
  faqSections:any=[];
  showLoading:any;
  loadingSubscription: Subscription;
  constructor(private service:Service) {
  }

  ngAfterViewInit() {
  }
  
  ngOnInit() {
    this.initializePreloading();
    this.getList();
  }

  //initialize prelaoding
  initializePreloading(){
    this.showLoading = true;
    this.loadingSubscription = this.service.loadingStatus.subscribe((value) => {
      this.showLoading = value;
    });
  }

  // get list
  getList(){
    let requestData = {
      domain:this.domainName,
      category:this.brandName
    };
    let queryString = this.service.convertObjecttoQueryString(requestData);
    this.service.getBase(this.apiSubUrl + queryString)
    .subscribe((responseData: any) => {
      if(responseData != null || responseData != undefined){
        this.service.stopLoading();
        let brandingData=[];
        responseData.forEach((element: { title: any; content: any; id:any}) => {
          let dataConstruct:Config = {
            title: element.title,
            description: element.content,
            id:element.id
          }
          brandingData.push(dataConstruct);
        });
        this.faqSections = brandingData;
      }else{
        this.service.startLoading();
      }
    });
  }
}
export interface Config {
  title:any;
  description:any
  id:any;
}