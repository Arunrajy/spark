import { Component, OnInit ,ViewChildren} from '@angular/core';
import { Service } from '../../service';
import { Router,ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
@Component({
  selector: 'app-topdomain',
  templateUrl: './topdomain.component.html',
  styleUrls: ['./topdomain.component.css']
})
export class TopdomainComponent implements OnInit {
  activeEnabled:boolean= true;
  topdomainall:any;
  domainItems:any = [];
  searchValue:any="";
  searchDomainUrl:string;
  message:string;
  searchapi:string = 'search-test';
  domainadded:any;
  domainnotadded:any;
  loader:boolean = true;
  @ViewChildren('input') vc;
  constructor(private service:Service,private router: Router,private activeroute: ActivatedRoute,private readonly location: Location) {
  }

  ngAfterViewInit() {
    this.vc.first.nativeElement.focus();
  }

  ngOnInit() {
    this.service.currentMessage.subscribe(message => this.message = message);
    this.activeroute.queryParamMap.subscribe(queryParams => {
      this.searchValue = queryParams.get("search")
      if(this.searchValue != null){
        this.onSubmit(this.searchValue);
        }else{
          this.router.navigate(['/']);
        }
    })
  }

  onKeyDown(event: any){
    if (event.which  === 32) { 
      return false;
    }
  }
  onKey(event: any) {
    if(!event.target.value){
      this.router.navigate(['/']);

    }else{
      let backspace = event.charCode || event.keyCode;
      if (backspace  === 32) { 
        return false;
      }
      if(!(event.altKey||event.metaKey||event.shiftKey||event.ctrlKey||event.keyCode === 37||event.keyCode === 38||event.keyCode === 39||event.keyCode === 40 )|| event.keyCode === 8){
        if (event.keyCode === 16) {return false;}
        if (event.keyCode === 17) {return false;}  
        if (event.keyCode === 91) {return false;}  

        this.searchDomainUrl=event.target.value;
      this.loader = true;

      // update queryparams
      this.service.updateQueryParams(this.searchDomainUrl, this.activeroute, this.location);

      let data = {
        passedDomain:this.searchDomainUrl
      };
      this.service.postConfig(this.searchapi,data)
      .subscribe((data: Config) => {

        if(data != null || data != undefined){
          this.loader = false;
          this.topdomainall = {
            domainAvailability: data['availability'],
            topdomain:  JSON.parse(data['cctld'])
        }
        }

      });

    }
  }

}

//for initial reload and submit
onSubmit(value){
  if(value.length != 0){
  this.loader = true;

  // update queryparams
  this.service.updateQueryParams(value, this.activeroute, this.location);

  let data = {
    passedDomain:value
  };
  this.service.postConfig(this.searchapi,data)
  .subscribe((data: Config) => {
    if(data != null){
      this.loader = false;
      this.topdomainall = {
        domainAvailability: data['availability'],
        topdomain: JSON.parse(data['cctld'])
    }
    }
  });
  }else{
    console.log("value is empty");
  }
  return false;
}

addtocart(domaindata:any){
  if(domaindata){
      if(localStorage.getItem('domainCartItems')!= null){
        this.domainItems = JSON.parse(localStorage.getItem('domainCartItems'));
        if(this.domainItems.indexOf(domaindata) === -1) {
          //add item
        this.domainItems.push(domaindata);
        }else{
          //remove item
          let removeditem = this.domainItems.splice(this.domainItems.indexOf(domaindata), 1);
        localStorage.setItem('domainCartItems', JSON.stringify(removeditem));
        }
        localStorage.setItem('domainCartItems', JSON.stringify(this.domainItems));
        this.service.changeMessage("Hello from Sibling")
      }else{
        this.domainItems.push(domaindata);
        this.domainadded = true;
        localStorage.setItem('domainCartItems', JSON.stringify(this.domainItems));
        this.service.changeMessage("Hello from Sibling")
      }

  }

}

localdomainname(data){
  if(localStorage.getItem('domainCartItems')!= null){
  let storedarray =  JSON.parse(localStorage.getItem('domainCartItems'))
   let included = storedarray.includes(data);
   return included
  }
}

}
export interface Config {
  availability: any;
  cctld: any;
}
