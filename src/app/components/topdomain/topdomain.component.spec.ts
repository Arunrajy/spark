import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopdomainComponent } from './topdomain.component';

describe('TopdomainComponent', () => {
  let component: TopdomainComponent;
  let fixture: ComponentFixture<TopdomainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopdomainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopdomainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
