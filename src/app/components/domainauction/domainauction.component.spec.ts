import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomainauctionComponent } from './domainauction.component';

describe('DomainauctionComponent', () => {
  let component: DomainauctionComponent;
  let fixture: ComponentFixture<DomainauctionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomainauctionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomainauctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
