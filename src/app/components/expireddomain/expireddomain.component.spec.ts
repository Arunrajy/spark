import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpireddomainComponent } from './expireddomain.component';

describe('ExpireddomainComponent', () => {
  let component: ExpireddomainComponent;
  let fixture: ComponentFixture<ExpireddomainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpireddomainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpireddomainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
