import { Component,OnInit } from '@angular/core';
import { trigger, transition, animate, style } from '@angular/animations'
import { Service } from './service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateY(-100%)'}),
        animate('200ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({transform: 'translateY(-100%)'}))
      ])
    ])
  ]
})

export class AppComponent implements OnInit {
  title = 'spark';
  domaincartlist:any=[];
  show: boolean= true;
  constructor(private service: Service) { }
  ngOnInit() {
    this.service.setScrollTop();
    //check cart is added or not
    if(localStorage.getItem('domainCartItems')!= null){
      this.domaincartlist = JSON.parse(localStorage.getItem('domainCartItems'));
    }else{
      this.domaincartlist = [];
  
    }

    //when clicking cart button it works
      this.service.currentMessage.subscribe(message => {
        if(localStorage.getItem('domainCartItems')!= null){
          this.domaincartlist = JSON.parse(localStorage.getItem('domainCartItems'));
          if(document.getElementById('detectcountchange')!= null){
          let targetNode = document.getElementById('detectcountchange');
          targetNode.classList.add("fadeIn");
          }
        }else{
          this.domaincartlist = [];

        }
      })
      
  }

  opencart(){
    this.show = !this.show;
  }

  removecartitem(domainitem){
    if(localStorage.getItem('domainCartItems')!= null){
    this.domaincartlist = JSON.parse(localStorage.getItem('domainCartItems'));
    for (let i = 0; i < this.domaincartlist.length; i++) {
      if (this.domaincartlist[i] === domainitem) { 
        this.domaincartlist.splice(i,1);
        break;
      }
    }
      localStorage.setItem('domainCartItems', JSON.stringify(this.domaincartlist));
    }
  }

  
}

