export const environment = {
  production: true,
  configUrl: 'https://speedydomainsearch.com/',
  configSubUrl: '/wp-json/api/speedy/',
  baseUrl: 'https://acestracms.networktoolset.com',
  placeHolderImage: 'https://via.placeholder.com/370x300.png'
};
